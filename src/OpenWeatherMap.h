#ifndef OpenWeatherMap_h
#define OpenWeatherMap_h

#include "Arduino.h"
#include "ArduinoJson.h"

class OpenWeatherMap {
    public: 
        OpenWeatherMap(String apiKey, String city);
        void updateData();
        String getIcon();
        String getCityName();
        float getTemperature();

    private:
        String _apiKey;
        String _city;
        JsonObject *_weatherData;
};

#endif