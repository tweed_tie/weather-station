#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <Wire.h>
#include "SH1106Wire.h"
#include "Config.h"
#include "WeatherStationFonts.h"
#include "OpenWeatherMap.h"

SH1106Wire display(0x3c, 12, 13);
OpenWeatherMap currentWeather(apiKey, city);

void setup() {
    Serial.begin(115200);

    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    Serial.print("Connecting to WiFi");
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }

    // initialize dispaly
    display.init();
    display.clear();
    display.display();

    display.flipScreenVertically();
    display.setContrast(255);

    currentWeather.updateData();
}

void drawFrame1() {
    display.setTextAlignment(TEXT_ALIGN_LEFT);
    display.setFont(ArialMT_Plain_16);

    display.drawString(6, 10, currentWeather.getCityName());
    display.drawString(6, 38, String(currentWeather.getTemperature()) + "°C");

    display.setTextAlignment(TEXT_ALIGN_CENTER);
    display.setFont(Meteocons_Plain_36);
    display.drawString(106, 14, currentWeather.getIcon());
}

void loop() {
    display.clear();

    drawFrame1();

    display.display();

    currentWeather.updateData();

    delay(5000);
}