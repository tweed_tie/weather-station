#include "Arduino.h"
#include "OpenWeatherMap.h"
#include "ESP8266HTTPClient.h"
#include "ArduinoJson.h"

OpenWeatherMap::OpenWeatherMap(String apiKey, String city) {
    _apiKey = apiKey;
    _city = city;
    _weatherData = nullptr;
}

void OpenWeatherMap::updateData() {
    StaticJsonBuffer<1200> jsonBuffer;
    HTTPClient http;

    String url = "http://api.openweathermap.org/data/2.5/weather?q=" + String(_city) + "&appid=" + String(_apiKey);
    http.begin(url);
    http.GET();

    _weatherData = &jsonBuffer.parseObject(http.getString());
}

float OpenWeatherMap::getTemperature() {
    return float((*_weatherData)["main"]["temp"]) - float(273.15);
}

String OpenWeatherMap::getCityName() {
    return (*_weatherData)["name"];
}

String OpenWeatherMap::getIcon() {
    String icon = (*_weatherData)["weather"][0]["icon"];

    // clear sky
    // 01d
    if (icon == "01d") 	{
        return "B";
    }
    // 01n
    if (icon == "01n") 	{
        return "C";
    }
    // few clouds
    // 02d
    if (icon == "02d") 	{
        return "H";
    }
    // 02n
    if (icon == "02n") 	{
        return "4";
    }
    // scattered clouds
    // 03d
    if (icon == "03d") 	{
        return "N";
    }
    // 03n
    if (icon == "03n") 	{
        return "5";
    }
    // broken clouds
    // 04d
    if (icon == "04d") 	{
        return "Y";
    }
    // 04n
    if (icon == "04n") 	{
        return "%";
    }
    // shower rain
    // 09d
    if (icon == "09d") 	{
        return "R";
    }
    // 09n
    if (icon == "09n") 	{
        return "8";
    }
    // rain
    // 10d
    if (icon == "10d") 	{
        return "Q";
    }
    // 10n
    if (icon == "10n") 	{
        return "7";
    }
    // thunderstorm
    // 11d
    if (icon == "11d") 	{
        return "P";
    }
    // 11n
    if (icon == "11n") 	{
        return "6";
    }
    // snow
    // 13d
    if (icon == "13d") 	{
        return "W";
    }
    // 13n
    if (icon == "13n") 	{
        return "#";
    }
    // mist
    // 50d
    if (icon == "50d") 	{
        return "M";
    }
    // 50n
    if (icon == "50n") 	{
        return "M";
    }
    // Nothing matched: N/A
    return ")";
}